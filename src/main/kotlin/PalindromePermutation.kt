fun isPalindromePermutation(s: String): Boolean {
    fun isPalindromePermutation(input: String): Boolean {
        val counts = mutableMapOf<Char, Int>()
        var oddCount = 0
        input.map {counts.set(it, counts.getOrDefault(it, 0) + 1) }
        counts.values.forEach {
            oddCount += it.rem(2)
            if (oddCount > 1) return false
        }
        return true
    }

    val input = s.replace("[^a-zA-Z0-9]".toRegex(), "")
    return when {
        input.isBlank() -> true
        else -> isPalindromePermutation(input)
    }
}
