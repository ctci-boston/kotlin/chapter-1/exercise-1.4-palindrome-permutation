import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class Test {
    @Test fun `Given the empty string verify true`() {
        val inputString = ""
        assertTrue(isPalindromePermutation(inputString))
    }

    @Test fun `Given all non relevant characters verify true`() {
        var tmpString = ""
        for (c in 0..127) { tmpString += c.toChar() }
        val inputString = tmpString.replace("[a-zA-Z0-9]".toRegex(), "") + "aa"
        assertEquals(128 - 62 + 2, inputString.length )
        assertTrue(isPalindromePermutation(inputString))
    }

    @Test fun `Given a single character verify true`() {
        val inputString = "x"
        assertTrue(isPalindromePermutation(inputString))
    }

    @Test fun `Given two different characters verify false`() {
        val inputString = "xy"
        assertFalse(isPalindromePermutation(inputString))
    }

    @Test fun `Given a simple palindrome verify true`() {
        val inputString = "xyx"
        assertTrue(isPalindromePermutation(inputString))
    }

    @Test fun `Given a non-trivial palindrome verify true`() {
        val inputString = "abcyyyyycba"
        assertTrue(isPalindromePermutation(inputString))
    }

    @Test fun `Given a non-trivial non-palindrome verify false`() {
        val inputString = "abcyyyyycbaz"
        assertFalse(isPalindromePermutation(inputString))
    }
}
