# exercise-1.4-palindrome-permutation

Provide a Kotlin solution for Exercise 1.4 Palindrome Permutation: given a string write a function to determine if it is a permutation of a palindrome.